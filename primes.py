#!/usr/bin/env python3



# constants
max_num = 500
columns = 10
max_row = 50


# main variables
primes = []
next_num = 0
next_probe = 0


primes.append( 2 )
next_num = 3

while  len( primes ) < max_num :
	next_probe = 0
	while True:
		if next_num % primes[ next_probe ] == 0:
			break
		elif primes[ next_probe ] ** 2 > next_num:
			primes.append( next_num )
			break
		else:
			next_probe += 1
		
	next_num += 2
	
	
print()
print( 'FIRST FIVE HUNDRED PRIMES' )

i = 0
while max_row:
	row = '    '
	col = columns
	while col:
		row += '{:1s}{:04d}'.format( '', primes[ i ] )
		i += 50
		col -= 1
	print( row )
	max_row -= 1
	i -= max_num - 1
	

